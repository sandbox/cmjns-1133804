<?php

function theme_filefield_storage_formatter_serving_url($file) {
  dpm($file);
  $storage = storage_api_file_load($file['#item']['data']['storage_api']['file_id']);
  if ($storage) {
    $url = storage_api_serve_url($storage, $absolute=TRUE);
    return l('Download', $url);
  }
  
  return t('This file currently is unavailable for download');
}